<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 09:22
 */

include "../controller/UserController.php";
include "../controller/GalleryController.php";
include "../controller/PictureController.php";

include "../model/User.php";
include "../model/Gallery.php";
include "../model/Picture.php";

include "../model/DBConnection.php";

class index
{
    private $galleryController;
    private $pictureController;
    function __construct()
    {
        $this->galleryController = new GalleryController();
        $this->pictureController = new PictureController();
        include '../php/header.php';

        $this->displayBegin();
        $this->displayGalleries();
        $this->displayEnd();

        include '../php/footer.php';
    }

    private function displayBegin(){
        echo '<div id="fh5co-main">
    <div class="fh5co-gallery">
        <i class="fas fa-eye"></i>';
    }

    private function displayEnd(){
        echo '    </div>
</div>';
    }

    private function displayGalleries(){

        foreach ($this->galleryController->getAllGallery() as $gallery){

            $images = $this->pictureController->getPicturesFromGallery($gallery->getId());
            $image = $this->pictureController->getPictureById($gallery->getFkImage());
            $imagecount = sizeof($images);

            echo '<a class="gallery-item" href="../GALLERY/index.php?galleryid=' . $gallery->getId() .'">';
            echo '<img src="../PICTURES/' . $image->getName() . '">';
            echo '<span class="overlay">';
            echo '<h2>' . $gallery->getName() . '</h2>';
            echo '<span>' . $imagecount . ' PHOTOS</span>';
            echo '<span><i class="icon-eye"></i> ' . $gallery->getViews() . '</span>';
            echo '</span>';
            echo '</a>';
        }
    }
}

new index();