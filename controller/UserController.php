<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 08:52
 */

class UserController
{
    private $dbConnection;

    function __construct()
    {
        $this->dbConnection = new DBConnection();
    }

    public function login($username, $password){
        $sql = "SELECT * FROM user WHERE username = '$username' AND password = '$password'";
        $result = $this->dbConnection->getConn()->query($sql);
        $user = null;
        if($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $user = new User($row['id'], $row['username'], $row['email'], $row['password'], $row['enabled']);
                return $user;
            }
        }
        return false;
    }

    public function getUserById($id){
        $sql = "SELECT * FROM user WHERE id = '$id'";
        $result = $this->dbConnection->getConn()->query($sql);
        $user = null;
        if($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $user = new User($row['id'], $row['username'], $row['email'], $row['password'], $row['enabled']);
                return $user;
            }
        }
        return false;
    }
}