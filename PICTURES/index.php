<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 09:48
 */

include "../controller/UserController.php";
include "../controller/GalleryController.php";
include "../controller/PictureController.php";

include "../model/User.php";
include "../model/Gallery.php";
include "../model/Picture.php";

include "../model/DBConnection.php";

class PictureView
{
    private $picture;
    private $pictureController;

    function __construct($picture_id)
    {
        $this->pictureController = new PictureController();
        $this->picture = $this->pictureController->getPictureById($picture_id);

        include '../php/header.php';

        $this->displayBegin();
        $this->displayPicture();
        $this->displayEnd();

        include '../php/footer.php';
    }

    private function displayPicture(){
        echo '<img class="fullImage" src="../PICTURES/' . $this->picture->getName() . '">';
    }

    private function displayBegin(){
        echo '<div id="fh5co-main">
    <div class="fh5co-gallery">';
    }

    private function displayEnd(){
        echo '</div>
</div>';
    }
}

if(isset($_GET["picture_id"])){
    new PictureView($_GET["picture_id"]);
} else {
    header("Location: ../HOME/");
}