<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 08:48
 */

class User
{
    private $id;
    private $username;
    private $email;
    private $password;
    private $enable;

    function __construct($id, $username, $email, $password, $enable)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->password = $password;
        $this->enable = $enable;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return mixed
     */
    public function getEnable()
    {
        return $this->enable;
    }
}