<?php
session_start();
include "../php/connection.php";
include '../model/User.php';

$galleryid = null;

$user = null;
if(isset($_SESSION["user"])){
    $user = unserialize($_SESSION["user"]);
} else {
    header("Location: ../HOME/");
}
$userid = $user->getId();

if(isset($_POST["galleryid"])){	
    $galleryid = $_POST["galleryid"];
} else {
	header("Location: ../HOME/");
}

if(!isset($_FILES['pictures'])){
    header("Location: ./index.php?galleryid=" . $galleryid);
}




//$files = array_filter($_FILES['upload']['name']); something like that to be used before processing files.
// Count # of uploaded files in array
$total = count($_FILES['pictures']['name']);
// Loop through each file
for($i=0; $i<$total; $i++) {


    $date = new DateTime();
    $timestamp = $date->getTimestamp() . rand(10000, 99999);

    $pathinfo = pathinfo($_FILES["pictures"]["name"][$i]);

    if(!(strtolower($pathinfo["extension"]) == "png" || strtolower($pathinfo["extension"]) == "jpg" || strtolower($pathinfo["extension"]) == "jpeg" || strtolower($pathinfo["extension"]) == "gif")){
        continue;
    }

    //Get the temp file path
    $tmpFilePath = $_FILES['pictures']['tmp_name'][$i];
    echo $tmpFilePath;

    //Make sure we have a filepath
    if ($tmpFilePath != ""){

        //Setup our new file path
        $newFilePath = "../PICTURES/" . $timestamp . "." . $pathinfo["extension"];

        //Upload the file into the temp dir
        if(move_uploaded_file($tmpFilePath, $newFilePath)) {
            $sql = "INSERT INTO picture (name, enabled, fk_user) VALUES ('" . $timestamp . "." . $pathinfo["extension"] . "', 1, $userid)";

            if ($conn->query($sql) === TRUE) {
                $sql = "SELECT id FROM picture WHERE name = '" . $timestamp . "." . $pathinfo["extension"] . "'";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        $sql = "INSERT INTO gallery_picture (fk_gallery, fk_picture) VALUES ($galleryid, " . $row["id"] . ")";
                        if ($conn->query($sql) === TRUE) {
                        }
                    }
                }
            }
        }
    }
}

header("Location: ./index.php?galleryid=" . $galleryid);