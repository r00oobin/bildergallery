<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 08:48
 */

class Picture
{
    private $id;
    private $name;
    private $timestamp;

    function __construct($id, $name, $timestamp)
    {
        $this->id = $id;
        $this->name = $name;
        $this->timestamp = $timestamp;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

}