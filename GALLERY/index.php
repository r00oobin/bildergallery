<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 09:39
 */

include "../controller/UserController.php";
include "../controller/GalleryController.php";
include "../controller/PictureController.php";

include "../model/User.php";
include "../model/Gallery.php";
include "../model/Picture.php";

include "../model/DBConnection.php";

class GalleryView
{
    private $galleryid;
    private $gallery;
    private $galleryController;
    private $pictureController;

    function __construct($galleryid)
    {
        $this->galleryController = new GalleryController();
        $this->pictureController = new PictureController();

        $this->galleryid = $galleryid;
        $this->gallery = $this->galleryController->getGalleryById($galleryid);

        include '../php/header.php';

        $this->displayBegin();
        $this->displayPictures();
        $this->displayEnd();

        include '../php/footer.php';

    }

    private function displayPictures(){
        $pictures = $this->pictureController->getPicturesFromGallery($this->galleryid);

        foreach ($pictures as $picture){
            echo '<a class="gallery-item" href="../PICTURES/index.php?picture_id=' . $picture->getId() .'">';
            echo '<img src="../PICTURES/' . $picture->getName() . '">';
            echo '<span class="overlay">';
            //$fileExtPos = $row2["name"].LastIndexOf(".");
            //if ($fileExtPos >= 0 )
            //   echo '<h2>' . $row2["name"].Substring(0, $fileExtPos) . '</h2>';
            $new_datetime = DateTime::createFromFormat ( "Y-m-d H:i:s", $picture->getTimestamp() );
            echo '<span>' . $new_datetime->format('l jS F Y') . '</span>';
            echo '</span>';
            echo '</a>';
        }

        if($pictures == false){
            echo " <span>no Pictures</span>";
        }
    }

    private function displayBegin(){
        echo '<div id="fh5co-main">
    <div class="fh5co-gallery">';
    }

    private function displayEnd(){
        if(isset($_SESSION["user"])){
            if($this->gallery->getFkUser() == unserialize($_SESSION["user"])->getId()){
                ?>
                    <form id="addFilesForm" action="./addPicture.php" method="post" enctype="multipart/form-data">
                        <input name="galleryid" value="<?php echo $this->galleryid; ?>" style="position: absolute; visibility: hidden;">
                        <input type="file" accept="image/*" name="pictures[]" class="addIcon" multiple id="inputFiles" style="position: absolute; visibility: hidden;">
                        <label class="addIcon" for="inputFiles">+</label>
                        
                    </form>
                <?php
            }
        }

        echo '</div>
</div>

<script>
    var inputFiles = document.getElementById("inputFiles");

    inputFiles.addEventListener("change", function () {
        document.getElementById("addFilesForm").submit();
    })
</script>';
    }
}


if(!isset($_GET['galleryid'])){
    header("Location: ../HOME/");
}
$galleryid = $_GET['galleryid'];
new GalleryView($galleryid);