<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 09:55
 */

include "../controller/UserController.php";
include "../controller/GalleryController.php";
include "../controller/PictureController.php";

include "../model/User.php";
include "../model/Gallery.php";
include "../model/Picture.php";

include "../model/DBConnection.php";

class UserView
{

    private $userController;

    private $username;
    private $password;
    private $login;

    function __construct($username = null, $password = null)
    {
        include '../php/header.php';
        $this->displayBegin();
        if($username != null && $password != null) {
            $this->username = $username;
            $this->password = $password;

            $this->userController = new UserController();
            $this->login = false;

            $user = null;
            if ($this->username != null && $this->password != null) {
                $user = $this->login();
            }

            if ($user === false) {
                echo "<h4 style='text-align: center; color: red;'>Couldn't log in. Username or password incorrect.</h4>";
            } else {
                if ($user->getEnable() == 0) {
                    echo "<h4 style='text-align: center; color: red;'>Couldn't log in. Username is not enable.</h4>";
                } else {
                    $_SESSION["user"] = serialize($user);
                    header("Location: ../HOME");
                }
            }
        }
        $this->displayEnd();
        include '../php/footer.php';
    }

    private function login(){
        return $this->userController->login($this->username, $this->password);
    }


    private function displayBegin(){
        echo '<div id="fh5co-main">
        <div class="fh5co-narrow-content animate-box" data-animate-effect="fadeInLeft">';
    }

    private function displayEnd(){
        echo '<form action="index.php" method="post">
                <div class="row login">
                    <h1>Login</h1>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <input type="text" name="username" class="form-control" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <a href="resetPassword.php">Forgot your password?</a>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-md" value="Log in">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>';
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }/**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}

if(isset($_POST['username']) && isset($_POST['password'])) {
    new UserView($_POST['username'], $_POST['password']);
} else {
    new UserView();
}
