<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 08:52
 */

class PictureController
{
    private $dbConnection;

    function __construct()
    {
        $this->dbConnection = new DBConnection();
    }

    public function getPicturesFromGallery($galleryId){
        $sql = "SELECT fk_picture FROM gallery_picture WHERE fk_gallery = '$galleryId'";
        $result = $this->dbConnection->getConn()->query($sql);
        $pictures = array();
        if($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                array_push($pictures, $this->getPictureById($row['fk_picture']));
            }
        }
        return $pictures;
    }

    public function getPictureById($id){
        $sql = "SELECT * FROM picture WHERE id = '$id'";
        $result = $this->dbConnection->getConn()->query($sql);
        if($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                return new Picture($row['id'], $row['name'], $row['timestamp']);
            }
        }
        return false;
    }

}