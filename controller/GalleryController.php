<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 08:52
 */

class GalleryController
{
    private $dbConnection;

    function __construct()
    {
        $this->dbConnection = new DBConnection();
    }

    public function getGalleryById($id){
        $sql = "SELECT * FROM gallery WHERE id = '$id'";
        $result = $this->dbConnection->getConn()->query($sql);
        $gallery = null;
        if($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $gallery = new Gallery($row['id'], $row['name'], $row['fk_user'], $row['timestamp'], $row['enabled'], $row['fk_image'], $row['Views']);
                $views = $row['Views'] + 1;
		$sql2 = "UPDATE gallery SET Views = $views WHERE id = " . $row['id'];
		$this->dbConnection->getConn()->query($sql2);
		return $gallery;
            }
        }
        return false;
    }

    /**
     * @return array Gallery
     */
    public function getAllGallery(){
        $sql = "SELECT * FROM gallery ORDER BY Views DESC";
        $result = $this->dbConnection->getConn()->query($sql);
        $galleries = array();
        if($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                array_push($galleries, new Gallery($row['id'], $row['name'], $row['fk_user'], $row['timestamp'], $row['enabled'], $row['fk_image'], $row['Views']));
            }
        }
        return $galleries;
    }
}
