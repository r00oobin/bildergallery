<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 08:54
 */

class DBConnection
{
    private $conn = null;

    private $servername = "localhost";
    private $username = "robin";
    private $password = "toor";
    private $dbname = "bildgallerie";

    function __construct()
    {
        if($this->conn == null){
            $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname);
            if ($this->conn->connect_error) {
                die("Connection failed: " . $this->conn->connect_error);
            }
        }
    }

    /**
     * @return mysqli
     */
    public function getConn()
    {
        return $this->conn;
    }
}