<?php
/**
 * Created by PhpStorm.
 * User: rfur
 * Date: 26.10.2018
 * Time: 09:08
 */

class Gallery
{
    private $id;
    private $name;
    private $fk_user;
    private $timestamp;
    private $enabled;
    private $fk_image;
    private $views;

    function __construct($id, $name, $fk_user, $timestamp, $enabled, $fk_image, $views)
    {
        $this->id = $id;
        $this->name = $name;
        $this->fk_user = $fk_user;
        $this->timestamp = $timestamp;
        $this->enabled = $enabled;
        $this->fk_image = $fk_image;
        $this->views = $views;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fk_user;
    }

    /**
     * @return mixed
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return mixed
     */
    public function getFkImage()
    {
        return $this->fk_image;
    }

    /**
     * @return mixed
     */
    public function getViews()
    {
        return $this->views;
    }



}